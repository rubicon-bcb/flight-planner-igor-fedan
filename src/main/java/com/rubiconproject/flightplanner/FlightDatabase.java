package com.rubiconproject.flightplanner;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * A flight database class that contains methods to obtain available flights from any airport as well as calculate
 * distances from any airport to any other airport.
 *
 *
 * @see Flight
 * @author erachitskiy
 */
public class FlightDatabase {
    private final Map<String, List<Flight>> flights;
    private final Map<String, Airport> airports;

    /**
     * Construct a {@link FlightDatabase}
     */
    public FlightDatabase() {
        try{
            flights = readFlights();
            airports = readAirports();
        }catch(Exception e){
            throw new RuntimeException("Could not initialize flight database",e);
        }
    }

    /**
     * Get all flights originating from supplied airport.  Note that flights cost a MINIMUM of their distance in miles
     * divided by two (in USD).
     *
     * @param airport an FAA airport code
     * @return list of all flights originating from airport code supplied
     */
    public List<Flight> getFlights(String airport) {
        List<Flight> ret = flights.get(airport);
        return ret==null?new LinkedList<Flight>():ret;
    }

    /**
     * Get the distance, in miles, between any two airports
     * @param src source airport FAA code
     * @param dst destination airport FAA code
     * @throws NullPointerException if source or destination airport codes could not be found in database
     * @return distance, in miles, between two airports
     */
    public double getDistance(String src, String dst) {
        Airport srcAirport = airports.get(src);
        Airport dstAirport = airports.get(dst);
        return Math.acos(
                Math.sin(srcAirport.getLatitude()/57.2958)*Math.sin(dstAirport.getLatitude()/57.2958)+
                Math.cos(srcAirport.getLatitude()/57.2958)*Math.cos(dstAirport.getLatitude()/57.2958)*
                Math.cos(dstAirport.getLongitude()/57.2958-srcAirport.getLongitude()/57.2958)
        )*3963.0;
    }


    /* utility methods */

    private Map<String, Airport> readAirports() throws IOException {
        Map<String, Airport> airports = new HashMap<String, Airport>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("ports.csv")));
        String line = null;

        while ((line = reader.readLine()) != null) {
            String[] fields = StringUtils.split(line, ",", 3);
            if (fields.length != 3) {
                continue;
            }
            Airport airport = new Airport(fields[0], Double.parseDouble(fields[1]), Double.parseDouble(fields[2]));
            airports.put(airport.getCode(), airport);
        }

        return airports;
    }

    private Map<String, List<Flight>> readFlights() throws IOException {
        Map<String, List<Flight>> flights = new HashMap<String, List<Flight>>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("routes.csv")));
        String line = null;
        while ((line = reader.readLine()) != null) {
            String[] fields = StringUtils.split(line, ",", 6);
            if (fields.length != 6) {
                continue;
            }
            Flight flight = new Flight(fields[0], fields[1], fields[2], fields[3], Double.parseDouble(fields[4]), Double.parseDouble(fields[5]));
            if(!flights.containsKey(flight.getSource())){
                flights.put(flight.getSource(),new ArrayList<Flight>());
            }
            flights.get(flight.getSource()).add(flight);
        }
        return flights;
    }

}
