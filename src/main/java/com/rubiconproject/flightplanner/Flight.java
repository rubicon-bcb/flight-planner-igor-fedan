package com.rubiconproject.flightplanner;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Immutable domain object representing a flight.  Note that a flight costs, in USD, a minimum of the total flight
 * mileage divided by half.
 *
 * @see {@code routes.csv}
 *
 * @author erachitskiy
 */
public class Flight {
    private final String id;
    private final String airline;
    private final String source;
    private final String destination;
    private final double distance;
    private final double cost;

    /**
     * Construct a flight
     * @param id flight id
     * @param airline flight airline
     * @param source flight source FAA airport code
     * @param destination flight destination FAA airport code
     * @param distance flight distance in miles
     * @param cost flight cost in USD
     */
    public Flight(String id, String airline, String source, String destination, double distance, double cost) {
        this.id = id;
        this.airline = airline;
        this.source = source;
        this.destination = destination;
        this.distance = distance;
        this.cost = cost;
    }

    /**
     * Get the unique flight identifier
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Get flight airline name
     * @return flight airline name
     */
    public String getAirline() {
        return airline;
    }

    /**
     * Get flight source FAA airport code
     * @return flight source FAA airport code
     */
    public String getSource() {
        return source;
    }

    /**
     * Get flight destination FAA airport code
     * @return flight destination FAA airport code
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Get total flight distance in miles
     * @return total flight distance in miles
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Get flight cost in USD
     * @return flight cost in USD
     */
    public double getCost() {
        return cost;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this,obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
