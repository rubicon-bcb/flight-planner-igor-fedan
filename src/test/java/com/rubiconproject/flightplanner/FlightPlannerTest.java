package com.rubiconproject.flightplanner;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;


/**
 * Tests for {@link FlightPlanner}
 *
 * @author erachitskiy
 */
public class FlightPlannerTest {
    FlightPlanner flightPlanner = new FlightPlanner();
    FlightDatabase flightDatabase = new FlightDatabase();

    /*
        TODO: make sure all tests pass
     */

    @Test
    public void testCLEtoLGA(){
        List<Flight> flights = flightPlanner.findRoute("CLE","LGA");
        assertValidRoute("CLE","LGA",flights);
    }

    @Test
    public void testLAXtoSYD(){
        List<Flight> flights = flightPlanner.findRoute("LAX","SYD");
        assertValidRoute("LAX","SYD",flights);
    }

    @Test
    public void testEWRtoAUS(){
        List<Flight> flights = flightPlanner.findRoute("EWR","AUS");
        assertValidRoute("EWR","AUS",flights);
    }

    @Test
    public void testFindComplicatedRoute(){
        /* find a route between SJO - San Jose Costa Rica, and WJU - WonJu South Korea */
        List<Flight> flights = flightPlanner.findRoute("SJO","WJU");
        assertValidRoute("SJO","WJU",flights);
    }

    /*
        TODO: add more tests here
     */


    private void assertValidRoute(String src,String dst,List<Flight> route){
        System.out.println("Route "+src+" to "+dst+":");
        assertNotNull("Expected a valid route, but route was null",route);
        assertFalse("Expected a valid route, got empty route", route.isEmpty());
        assertEquals("Expected source airport "+src+" was "+route.get(0).getSource(),src,route.get(0).getSource());
        assertEquals("Expected destination airport "+dst+" was "+route.get(route.size()-1).getDestination(),dst,route.get(route.size()-1).getDestination());
        double cost = 0;
        for(Flight flight:route){
            boolean flightExists = false;
            for(Flight dstFlight:flightDatabase.getFlights(flight.getSource())){
                if(dstFlight.equals(flight)){
                    flightExists=true;
                    break;
                }
            }
            assertTrue("Flight "+flight+" does not exist",flightExists);
            System.out.println(" "+flight);
            cost+=flight.getCost();
        }
        System.out.println("Total Cost:"+cost);
        System.out.println();
    }
}
